import ui.Menu;

public class Main {
    /*
    * Starts the program when creating a Menu object with the given path,
    * and then the main function starts the menu when calling at the start function.*/
    public static void main(String[] args) {
        Menu menu = new Menu(System.getProperty("user.dir"));
        menu.start();
    }
}