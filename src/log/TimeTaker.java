package log;

public class TimeTaker {
    private long fromTime = Long.MAX_VALUE; // To check if the fromTime are set.
    private long toTime = 0;

    public void start(){
        fromTime = System.nanoTime();
    }

    public int end(){
        toTime = System.nanoTime();
        if (fromTime == Long.MAX_VALUE){
            System.out.println("ERROR TimeTaker: start() must be called before end()");
            return -1;
        }
        return (int)(toTime - fromTime) / 1000000;
    }
}
