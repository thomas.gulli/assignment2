package log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {
    private FileWriter fileWriter;
    private String outPath;
    public boolean closed;

    /* The constructor takes a path for the logfile to be placed.
     * If the directory is not made, the constructor will try to make the folder.*/
    public Log(String outPath){
        this.outPath = outPath;
        closed = false;

        makeDirectories();

        try {
            fileWriter = new FileWriter(outPath);
        } catch (IOException e) {
            System.out.println("Failed to create writer for logging " + e.toString());
        }
    }

    // The program makes directories for the log file, if the directory not exist.
    private void makeDirectories(){
        String outFolderStr = outPath.substring(0, outPath.lastIndexOf("\\"));
        String filename = outPath.substring(outPath.lastIndexOf("\\")+1);
        File outFolder = new File(outFolderStr);

        if (!outFolder.exists() || !outFolder.isDirectory()){
            String filePath = outFolderStr + "\\" + filename;
            try {
                Files.createDirectories(Paths.get(outFolderStr));
                outPath = filePath;
            } catch (IOException e) {
                outPath = System.getProperty("user.dir") + "\\" + filename;
            }
        }
    }

    // Logs the string, and the time it took to run the function, durationTime
    public void log(String str, int durationTime){
        try {
            fileWriter.write(new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss:SS z")
                    .format(new Date(System.currentTimeMillis())) + ":\n");
            fileWriter.write(str + "\n");
            fileWriter.write("The function took " + durationTime + " ms.\n\n");

        } catch (IOException e) {
            System.out.println("Failed to write in log " + e.toString());
        }
    }

    //Prints and logs the string with duration time.
    public void logAndPrint(String str, int durationTime){
        System.out.println(str);
        log(str, durationTime);
    }

    public void closeWriter(){
        try {
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Failed to close writer for logging " + e.toString());
            e.printStackTrace();
        }
        closed = true;
    }
}
