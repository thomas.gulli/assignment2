package ui;

import filemanager.FileManager;
import log.Log;
import java.util.Scanner;

public class Menu {
    // Constant for showing all files, or just the allowed ones.
    final static private String ALL = "all";
    final static private String ONLYALLOWED = "onlyallowed";

    final private FileManager resourcesFolder; // FileManager with path to resources folder.
    final private Log log;
    public Scanner fromTerminal;

    public Menu(String startPath){
        log = new Log(startPath + "\\out\\log.txt");
        resourcesFolder = new FileManager(startPath + "\\resources", log);
    }

    /*
    * Recursive menu function that will call it self at the end. To return out of the recursion,
    * the user have to write q in the terminal.
    * The menu function makes the user to choose what the program should do, and call the right functions.*/
    public void start(){
        fromTerminal = new Scanner(System.in);
        System.out.println("\n\n########### Main menu ###########");
        System.out.println("For navigate in the menu, write the number in front of the command.");
        System.out.println("To quit the program, you can always write q.\n");
        System.out.println("1. Prints the allowed file types.");
        System.out.println("2. Prints all files in resources folder.");
        System.out.println("3. Prints only the allowed files in the folder.");
        System.out.println("4. Prints all files with a given filetype.");
        System.out.println("5. Manipulate Dracula.txt.");
        System.out.println("6. Manipulate a given txt file.");

        String str = fromTerminal.nextLine();

        if (checkForQuitSymbol(str)){ // if user writes q for quit
            log.closeWriter();
            return;

        } else if (str.matches("[1-6]")){
            int input = Integer.parseInt(str);

            switch (input) {
                case 1:
                    resourcesFolder.printAllowedFileTypes();
                    break;
                case 2:
                    resourcesFolder.printFiles(ALL);
                    break;
                case 3:
                    resourcesFolder.printFiles(ONLYALLOWED);
                    break;
                case 4:
                    if (!printGivenType()){ // if false, then quit the program
                        log.closeWriter();
                        return;
                    }
                    break;
                case 5:
                    if (!manipulateMenu("Dracula.txt")) {
                        log.closeWriter();
                        return;
                    }
                    break;
                case 6:
                    if (!manipulateMenu("")) {
                        log.closeWriter();
                        return;
                    }
                    break;
            }
        } else if (!checkForMenuSymbol(str)){
            System.out.println("Wrong input. See menu for valid commands.");
        }
        start();
    }


    private boolean checkForQuitSymbol(String s){
        return s.equalsIgnoreCase("q");
    }

    private boolean checkForMenuSymbol(String s){
        return s.equalsIgnoreCase("m");
    }

    private boolean printGivenType(){
        System.out.println("Write the type of the file to print.");
        String type = fromTerminal.nextLine();

        if (checkForQuitSymbol(type)){
            return false;
        } else if (checkForMenuSymbol(type)){
            return true;
        }
        resourcesFolder.printFiles(type);
        return true;
    }

    /*
    * Function that is a menu for manipulating files
    * At default, the parameter manipFile is "". When a specific file should be manipulated, then set manipFile to that file.
    * If manipFiles is "quit", the programs quit. If manipFiles is "menu", the program goes back to the main menu.
    * So the function returns true when the program should go bask to the main menu.
    * If the program should quit, then the function returns false.
    * */
    private boolean manipulateMenu(String manipFile){
        if (manipFile.equals("")){
            System.out.println("Write the filename for the txt file to be manipulated. (include .txt at the end)");
            manipFile = getFilePath();
        }
        if (manipFile.equals("quit")){
            return false;
        } else if (manipFile.equals("menu")){
            return true;
        }
        FileManager fileToMan = new FileManager(System.getProperty("user.dir") + "\\resources\\" + manipFile, log);

        System.out.println("\n¤¤¤¤¤¤ Manipulate menu ¤¤¤¤¤¤");
        System.out.println("File: " + fileToMan.getName());
        System.out.println("Path: " + fileToMan.getPath());
        System.out.println("If you want back to the main menu, write m. To quit the program write q\n");
        System.out.println("1. Prints the name of the file.");
        System.out.println("2. Prints the file size.");
        System.out.println("3. Prints number of lines in the file.");
        System.out.println("4. Search for a specific word in the file.");
        String command = fromTerminal.nextLine();

        if (checkForQuitSymbol(command)){
            return false;
        } else if (checkForMenuSymbol(command)){
            return true;
        } else if (command.matches("[1-4]")) {
            int input = Integer.parseInt(command);
            String printString = "Manipulate file " + fileToMan.getName() + " at path:\n";
            printString += "Path: " + fileToMan.getPath() + "\n\n";

            switch (input) {
                case 1:
                    fileToMan.printName(printString);
                    break;
                case 2:
                    fileToMan.printNBytes(printString);
                    break;
                case 3:
                    fileToMan.printNLines(printString);
                    break;
                case 4:
                    if (!searchForWord(fileToMan, printString)) {
                        return false;
                    }
                    break;
            }
        } else {
            System.out.println("Wrong input. See menu for valid commands.");
        }
        return manipulateMenu(manipFile);
    }

    /*
    * Searching for a word in a file, and prints number of occurrences of that word.
    * The word comes from terminal. The parameter printString is the filename and the path for the file for printing.
    * Returns false if the program should quit.*/
    private boolean searchForWord(FileManager file, String printString){
        System.out.println("Write the word to search for in the file:");
        String word = fromTerminal.nextLine();

        if (checkForQuitSymbol(word)){
            return false;
        } else if (checkForMenuSymbol(word)){
            return true;
        }

        file.printNOccurrences(word, printString);
        return true;
    }

    /*
     * Function gets the path for a new file.
     * Returns false if the program should quit.*/
    private String getFilePath(){
        String manipFile = fromTerminal.nextLine();
        manipFile = manipFile.toLowerCase();

        if (checkForQuitSymbol(manipFile)){
            return "quit";
        } else if (checkForMenuSymbol(manipFile)){
            return "menu";
        }
        if (!resourcesFolder.checkTxtFile(manipFile)){
            System.out.println("ERROR: Wrong filename. (Only txt files is allowed, include .txt at the end) Please write correct filename:");
            System.out.println("(You can also quit the program with q, or go back to the manipulate menu with m)");
            return getFilePath();
        }
        return manipFile;
    }
}
