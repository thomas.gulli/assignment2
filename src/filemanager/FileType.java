package filemanager;

public enum FileType {
    txt,
    png,
    jpg,
    jpeg,
    jfif
}
