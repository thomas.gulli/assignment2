package filemanager;

import log.Log;
import log.TimeTaker;

import java.io.*;
import java.util.Arrays;

public class FileManager {
    // Constant for showing all files, or just the allowed ones.
    final private String ALL = "all";
    final private String ONLYALLOWED = "onlyallowed";

    final private String[] allowedFileTypes;
    final private File file;
    final private Log log;
    final private TimeTaker myTimer;

    private BufferedReader buffReader;
    private FileReader fileReader;

    /*
    * Constructor needs a path to a file, and a Log object for logging.
    * The constructor also makes a string array with allowed filetypes from the FileType.java enum.*/
    public FileManager(String strFile, Log log){
        this.file = new File(strFile);
        this.log = log;

        allowedFileTypes = new String[(FileType.values()).length];
        FileType[] filetypes = FileType.values();
        myTimer = new TimeTaker();

        for (int i = 0; i < allowedFileTypes.length; i++){
            allowedFileTypes[i] = filetypes[i].toString();
        }
    }

    public String getName(){
        return file.getName();
    }

    /*
    * Prints the filename. Including some introduction string for nice prints as parameter.*/
    public void printName(String printString){
        myTimer.start();
        printString += "Filename: " + getName() +"\n";
        int time = myTimer.end();
        log.logAndPrint(printString, time);
    }

    public String getPath(){
        return file.getPath();
    }

    public File getFile(){
        return file;
    }

    /*
     * Prints the number of bytes in file. Including some introduction string for nice prints as parameter.*/
    public void printNBytes(String printString){
        myTimer.start();
        printString += "The file has " + file.length() + " bytes.\n";
        int time = myTimer.end();
        log.logAndPrint(printString, time);
    }

    /*
     * Prints the number of lines in a file. Including some introduction string for nice prints as parameter.*/
    public void printNLines(String printString){
        myTimer.start();
        int time;
        if (!openReader()){
            time = myTimer.end();
            printString += "Error when open buffreader for reading number of liens in file.\n";
            log.logAndPrint(printString, time);
            return;
        }

        int lineNumbers = 0;
        try{
            while (buffReader.readLine() != null){
                lineNumbers++;
            }
        } catch (IOException e){
            time = myTimer.end();
            printString += "Error: IOException when reading number of liens in file.\n";
            printString += e.toString() + "\n";
            log.logAndPrint(printString, time);
            return;
        }

        closeReader();
        if (lineNumbers > 0) {
            printString += "The file has " + lineNumbers + " lines.\n";
        } else{
            printString += "ERROR: could not count lines in file " + getName() + "\n";
        }
        time = myTimer.end();
        log.logAndPrint(printString, time);
    }

    /*
     * Prints the number of times a word is in a file. Including some introduction string for nice prints as parameter.*/
    public void printNOccurrences(String word, String printString){
        myTimer.start();

        int occurrences = 0;
        openReader();

        try {
            String line = buffReader.readLine();
            while (line != null) {
                String[] splitLine = line.split("[\\W&&\\D]+");
                for (String loopWord : splitLine) {
                    if (word.equalsIgnoreCase(loopWord)) {
                        occurrences++;
                    }
                }

                line = buffReader.readLine();
            }
        } catch (IOException e){
            printString += "ERROR when searching for occurences:\n" + e.toString() + "\n";
            return;
        }

        if (occurrences > 0){
            printString += "The word \"" + word + "\" is found " + occurrences + " times.\n";
        } else if (occurrences == 0) { // IOException
            printString += "The word \"" + word + "\" is not found in the file.\n";
        } else{
            printString += "ERROR: IOException\n";
        }

        int time = myTimer.end();
        log.logAndPrint(printString, time);
    }

    private boolean openReader(){
        try{
            fileReader = new FileReader(getPath());
        } catch (FileNotFoundException e){
            System.out.println("ERROR: The file is not found. " + e.toString());
            return false;
        }
        buffReader = new BufferedReader(fileReader);
        return true;
    }

    private void closeReader(){
        try{
            buffReader.close();
        } catch (IOException e){
            System.out.println("ERROR: Could not close buffReader. " + e.toString());
        }
        try{
            fileReader.close();
        } catch (IOException e){
            System.out.println("ERROR: Could not close fileReader. " + e.toString());
        }
    }

    public boolean checkTxtFile(String fileName){
        if (!fileName.endsWith(".txt")){
            return false;
        }

        if (file == null){
            return false;
        }

        File[] filesInFolder = file.listFiles();
        if (filesInFolder == null){
            System.out.println("ERROR: Could not find " + fileName + " in the folder.");
            return false;
        }
        for (File f : filesInFolder) {
            if (f.getName().equalsIgnoreCase(fileName)){
                return true;
            }
        }
        return false;
    }

    public void printAllowedFileTypes() {
        myTimer.start();
        String str = "Allowed file types: " + Arrays.toString(allowedFileTypes);
        int time = myTimer.end();
        log.logAndPrint(str, time);
    }

    /*Calls ther ecursion function getFilesInDirectory for printing all the files at the path, including  their subfolders.
    * If type is all, all files will be printed.
    * If type is allowed, only allowed files will be printed.
    * If type is something else, it should be a filetype, it will then print all the files with the given type.*/
    public void printFiles(String type){
        myTimer.start();
        String str = getFilesInDirectory(file, 0, type);
        int time = myTimer.end();
        log.logAndPrint(str, time);
    }

    /* Finds all files with given type, including the subfolders recursively.
    * The parameter file is from the file to search from.
    * nRecursions is for adding whitespaces for print out.
    * Type is the allowed type that should be printed out*/
    private String getFilesInDirectory(File file, int nRecursions, String type){
       if (type.charAt(0) == '.'){ //Allows types with and without the "." (f. ex the type .txt and txt)
           type = type.substring(1);
       }

        // check if the filetype is allowed or not.
        if (!checkFileType(type)){
            return "The type \"" + type + "\" is not supported.";
        }

        File[] filesInFolder = file.listFiles();
        if (filesInFolder == null){
            return "ERROR: Could not find " + file.getPath() + " in the folder.";
        }
        StringBuilder returnString = new StringBuilder(type + " files at path \"" + file.getPath() + "\":\n");

        for (File f : filesInFolder) {
            if (f.isFile()) {
                returnString.append(getOneFile(f.getName(), type, nRecursions)); // Get one file only returns allowed types of file
            } else { //Folder
                returnString.append(addWhitespace(f.getName(), nRecursions)).append(":\n");
                nRecursions++;
                returnString.append(getFilesInDirectory(f, nRecursions, type));
                nRecursions--;
            }
        }
        return returnString.toString();
    }

    /*
    * Get one file with the specified type from a filename*/
    private String getOneFile(String filename, String type, int nRecursions){
        String returnString = "";

        if (type.equals(ALL)){
            returnString = addWhitespace(filename, nRecursions)+"\n";
        } else if (type.equals(ONLYALLOWED)){
            for (String s : allowedFileTypes) {
                if (filename.endsWith("." + s)) {
                    returnString += addWhitespace(filename, nRecursions) + "\n";
                    break;
                }
            }
        } else{ // a given type
            if (filename.endsWith("." + type)) {
                returnString = addWhitespace(filename, nRecursions) + "\n";
            }
        }
        return returnString;
    }

    /*Adding whitespace/ a tab so many times the recursion has gone*/
    private String addWhitespace(String fileType, int nRecursions){
        String whiteSpace = "\t".repeat(nRecursions);
        return (whiteSpace + fileType);
    }

    /*Checks a filetype is allowed or not*/
    private boolean checkFileType(String type){
        if(type.equals(ALL) || type.equals(ONLYALLOWED)) {
            return true;
        }
        for (String fileType : allowedFileTypes) {
            if (fileType.equalsIgnoreCase(type)) {
                return true;
            }
        }
        return false;
    }

}
